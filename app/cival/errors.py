
import sys, os

from . import app

@app.errorhandler(500)
def internal_server_error(error):
	return error, 500

@app.errorhandler(404)
def page_not_found(error):
	return error, 404
