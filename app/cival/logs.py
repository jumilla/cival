
import sys, os
import logging, logging.handlers

from . import app



log_formatter = logging.Formatter(
	'%(asctime)s %(levelname)s: %(message)s '
	'[in %(pathname)s:%(lineno)d]'
)

# logディレクトリを作成する
if not os.path.exists('logs'):
	os.mkdir('logs')

# DEBUGログ用のハンドラを追加する
debug_file_handler = logging.handlers.RotatingFileHandler(
	'logs/debug.log', maxBytes=100000, backupCount=10
)
debug_file_handler.setLevel(logging.DEBUG)
debug_file_handler.setFormatter(log_formatter)
app.logger.addHandler(debug_file_handler)

# ERRORログ用のハンドラを追加する
error_file_handler = logging.handlers.RotatingFileHandler(
	'logs/error.log', maxBytes=100000, backupCount=10
)
error_file_handler.setLevel(logging.ERROR)
error_file_handler.setFormatter(log_formatter)
app.logger.addHandler(error_file_handler)

app.logger.setLevel(logging.INFO)
app.logger.info('wakeup')
app.logger.info('request uri: ' + os.environ['REQUEST_URI'])

# システムの文字エンコーディングをチェックする
if sys.getdefaultencoding() != 'utf-8':
	app.logger.error('sys.defaultencoding: ' + sys.getdefaultencoding())
	raise Exception('invalid system encoding')
if sys.getfilesystemencoding() != 'utf-8':
	app.logger.error('sys.filesystemencoding: ' + sys.getfilesystemencoding())
	raise Exception('invalid system encoding')
