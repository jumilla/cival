
import sys, os
import flask



version = '0.1'

# flaskアプリケーションを作成する
app = flask.Flask(__name__)
#app.debug = True

from . import logs

from . import views
from . import edit
from . import search
from . import download
from . import errors



if __name__ == '__main__':
    app.run()
