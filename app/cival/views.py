
import sys, os
import flask
import markdown
import jinja2

from . import app



# 設定ファイルを読み込む
TEMPLATE_FILENAME = '../html5.jinja2'
CONTENTS_MENU_FILENAME = 'menu.md'
options = {
	'input': None,
	'output_format': 'html5',
	'extensions': ['nl2br'],
}



def load_file(filePath, default = None):
	try:
		with open(filePath, encoding='UTF-8') as f:
			return f.read()
	except:
		return default



def loadPage(documentName, pageName):
	APP_URL_PATH = os.path.dirname(os.path.dirname(flask.request.url_root))
	ROOT_URL_PATH = os.path.dirname(APP_URL_PATH)

	# Read & Convert to HTML
#	if 'PATH_TRANSLATED' in os.environ:
#		markdownFilePath = os.environ['PATH_TRANSLATED'];
#		app.logger.debug('PATH_TRANSLATED: ' + os.environ['PATH_TRANSLATED'])
#	elif len(sys.argv) > 1:
#		markdownFilePath = sys.argv[1]
	contentsDirectoryPath = '../' + documentName
	menuFilePath = os.path.join(contentsDirectoryPath, CONTENTS_MENU_FILENAME)
	markdownFilePath = os.path.join(contentsDirectoryPath, pageName + '.md')
	app.logger.debug(menuFilePath)
	app.logger.debug(markdownFilePath)

	fileTitle, ext = os.path.splitext(os.path.basename(markdownFilePath))
	menuMarkdownString = load_file(menuFilePath, '')
	contentMarkdownString = load_file(markdownFilePath, '')
	app.logger.debug('contentMarkdownString: ' + contentMarkdownString)

	templateParameters = {
		'rootUrl': ROOT_URL_PATH + '/',
		'title': documentName,
		'menu': markdown.markdown(menuMarkdownString, **options),
		'content': markdown.markdown(contentMarkdownString, **options),
	}

	template = jinja2.Template(load_file(TEMPLATE_FILENAME, ''))
	htmlString = template.render(**templateParameters)

	return htmlString



@app.route('/')
def allPage():
	app.logger.debug('allPage')
	try:
		return loadPage('contents1', 'Home')
	except Exception as e:
		app.logger.exception(e)
		return 'Error: ' + str(e)

@app.route('/<documentName>/')
def documentRootPage(documentName):
	app.logger.debug('documentRootPage')
	try:
		return loadPage(documentName, 'Home')
	except Exception as e:
		app.logger.exception(e)
		return 'Error: ' + str(e)

@app.route('/<documentName>/<path:fragmentPath>')
def showFlagment(documentName, fragmentPath):
	app.logger.debug('showFlagment')
	components = fragmentPath.split('/')
	documentName = documentName
	pageName = components[0]
#	app.logger.debug('documentName:' + documentName)
#	app.logger.debug('pageName:' + pageName)
	try:
		return loadPage(documentName, pageName)
	except Exception as e:
		app.logger.exception(e)
		return 'Error: ' + str(e)

