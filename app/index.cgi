#!/usr/local/bin/python3

import sys, os

#sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

from wsgiref.handlers import CGIHandler
from cival import app

CGIHandler().run(app)
